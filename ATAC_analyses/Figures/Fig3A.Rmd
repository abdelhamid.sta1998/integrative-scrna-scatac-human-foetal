---
title: "Figure 3A"
output: html_notebook
---

```{r}
library(Seurat)
library(tidyverse)
library(lemon)
library(forcats)

```

```{r}
# loading objects
test = readRDS("/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.ChromVar.Integr3kb.Rds")




# plot the clustering as we want 
clusterplot = DimPlot(test, pt.size = 0.1) + ggplot2::ggtitle("Harmony integration")
df.cluster = clusterplot$data
levels(df.cluster$ident)[levels(df.cluster$ident)=="0"] <- "7"
df.cluster$ident = fct_relevel(df.cluster$ident, as.character(seq(1:7)))

```

```{r}
save = F
plot3B = ggplot(df.cluster, aes(x = UMAP_1, y = UMAP_2, colour = ident)) + 
  geom_point(size = 1, alpha = 1) + 
  scale_colour_manual(values = c( '#3CB44B', '#FFE119', '#4363D8', '#F58231', '#911EB4', '#46F0F0','#E6194B')) + 
  xlab("UMAP1") + ylab("UMAP2") +
  theme(panel.border=element_blank(), 
        axis.line = element_line(colour = 'black', size = 1), 
        axis.ticks = element_blank(),
        axis.text = element_blank(), 
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.text=element_text(size=12)) +  
  guides(color = guide_legend(override.aes = list(size=5))) + 
  coord_capped_cart(bottom='right', left='none') 

if (save) {
  cairo_pdf(file = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/final/UMAPATAC.pdf",width = 8, height = 6)
  print(plot3B)
  dev.off()
} else {
  print(plot3B)
}


```

