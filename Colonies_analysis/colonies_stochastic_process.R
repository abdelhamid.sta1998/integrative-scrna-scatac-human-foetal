### main code for the colonies analysis
### developer: Irina Mohorianu
### Jan - Mar 2020

##setwd("~/Dropbox/CSCI/AnaCvejic/colonies_project")
source("colonies_stochastic_process_functions.R")

## Mouse data
e1.inp <- read.csv("e1_mouse.csv",sep=',',header=F)
e1.mouse = process.input(e1.inp, 20)
e2.inp <- read.csv("e2_mouse.csv",sep=',',header=F)
e2.mouse = process.input(e2.inp, 20)
e3.inp <- read.csv("e3_mouse.csv",sep=',',header=F)
e3.mouse = process.input(e3.inp, 20)

##Human data
e2.inp <- read.csv("e2_human.csv",sep=',',header=F)
e2.human = process.input(e2.inp, 20)
e3.inp <- read.csv("e3_human.csv",sep=',',header=F)
e3.human = process.input(e3.inp, 20)
e4.inp <- read.csv("e4_human.csv",sep=',',header=F)
e4.human = process.input(e4.inp, 20)

e1.m.sum  = calculate.summary(e1.mouse, label = "e1.mouse")
e1.chi    = calculate.chi(e1.mouse, my.label = "e1_mouse_chisq.pdf")
e1.fisher = calculate.fisher(e1.mouse, my.label = "e1_mouse_fisher.pdf")

e2.m.sum  = calculate.summary(e2.mouse, label = "e2.mouse")
e2.chi    = calculate.chi(e2.mouse, my.label = "e2_mouse_chisq.pdf")
e2.fisher = calculate.fisher(e2.mouse, my.label = "e2_mouse_fisher.pdf")

e3.m.sum  = calculate.summary(e3.mouse, label = "e3.mouse")
e3.chi    = calculate.chi(e3.mouse, my.label = "e3_mouse_chisq.pdf")
e3.fisher = calculate.fisher(e3.mouse, my.label = "e3_mouse_fisher.pdf")


e2.h.sum  = calculate.summary(e2.human, label = "e2.human")
e2.chi    = calculate.chi(e2.human, my.label = "e2_human_chisq.pdf")
e2.fisher = calculate.fisher(e2.human, my.label = "e2_human_fisher.pdf")

e3.h.sum  = calculate.summary(e3.human, label = "e3.human")
e3.chi    = calculate.chi(e3.human, my.label = "e3_human_chisq.pdf")
e3.fisher = calculate.fisher(e3.human, my.label = "e3_human_fisher.pdf")

e4.h.sum  = calculate.summary(e4.human, label = "e4.human")
e4.chi    = calculate.chi(e4.human, my.label = "e4_human_chisq.pdf")
e4.fisher = calculate.fisher(e4.human, my.label = "e4_human_fisher.pdf")

##collate the summaries from the Fisher with RUs

collated.summary <- cbind(e1.m.sum, e2.m.sum[,2:3], e3.m.sum[,2:3],
                          e2.h.sum[,2:3], e3.h.sum[,2:3], e4.h.sum[,2:3])

colnames(collated.summary) = c("Exp", "e1.m.obs", "e1.m.FET","e2.m.obs", "e2.m.FET", "e3.m.obs", "e3.m.FET",
                               "e2.h.obs", "e2.h.FET", "e3.h.obs", "e3.h.FET", "e4.h.obs", "e4.h.FET")

FET.matrix = collated.summary[,c(3,5,7,9,11,13)]
write.csv(collated.summary, file = "FET_collatedSummary.csv")

FET.melt = c("type", "obs", "FET", "exp","org")
for(i in 1:nrow(collated.summary))
{
  add.melt1 = c(rownames(collated.summary)[i], collated.summary[i,2],collated.summary[i,3],"e1","mmu")
  add.melt2 = c(rownames(collated.summary)[i], collated.summary[i,4],collated.summary[i,5],"e2","mmu")
  add.melt3 = c(rownames(collated.summary)[i], collated.summary[i,6],collated.summary[i,7],"e3","mmu")
  FET.melt = rbind(FET.melt, add.melt1)
  FET.melt = rbind(FET.melt, add.melt2)
  FET.melt = rbind(FET.melt, add.melt3)
  add.melt1 = c(rownames(collated.summary)[i], collated.summary[i,8],collated.summary[i,9],"e2","hsa")
  add.melt2 = c(rownames(collated.summary)[i], collated.summary[i,10],collated.summary[i,11],"e3","hsa")
  add.melt3 = c(rownames(collated.summary)[i], collated.summary[i,12],collated.summary[i,13],"e4","hsa")
  FET.melt = rbind(FET.melt, add.melt1)
  FET.melt = rbind(FET.melt, add.melt2)
  FET.melt = rbind(FET.melt, add.melt3)
}
FET.melt = FET.melt[2:nrow(FET.melt),]
colnames(FET.melt) = c("type", "obs", "FET", "exp","org")
FET.melt.df = data.frame(FET.melt[,1],
                         as.numeric(as.character(FET.melt[,2])),
                         as.numeric(as.character(FET.melt[,3])),
                         FET.melt[,4], FET.melt[,5]) 
colnames(FET.melt.df) = c("type", "obs", "FET", "exp","org")
pdf("FET_RU_30.pdf", width = 10, height = 5)
ggplot(FET.melt.df, aes(x=org,y=obs))+
  geom_boxplot() + ggtitle("normalised number of colonies")+
  facet_grid(cols = vars(type))
ggplot(FET.melt.df, aes(x=org,y=FET, col=exp))+
    geom_jitter() + ggtitle("Fisher Exact test on RU for colonies")+
  facet_grid(cols = vars(type))
dev.off()


e1.m.sum <- summary(e1.mouse$new.lab)
e2.m.sum <- summary(e2.mouse$new.lab)
e3.m.sum <- summary(e3.mouse$new.lab)

e1.m.sum / sum(e1.m.sum)
e2.m.sum / sum(e2.m.sum)
e3.m.sum / sum(e3.m.sum)

e2.h.sum <- summary(e2.human$new.lab)
e3.h.sum <- summary(e3.human$new.lab)
e4.h.sum <- summary(e4.human$new.lab)

e2.h.sum / sum(e2.h.sum)
e3.h.sum / sum(e3.h.sum)
e4.h.sum / sum(e4.h.sum)

##create the melted version for the number of cells.
e1.mouse = cbind(e1.mouse, rep("e1",nrow(e1.mouse)), rep("mmu",nrow(e1.mouse)))
colnames(e1.mouse)[ncol(e1.mouse)-1] = "label"
colnames(e1.mouse)[ncol(e1.mouse)]   = "org"
e2.mouse = cbind(e2.mouse, rep("e2",nrow(e2.mouse)), rep("mmu",nrow(e2.mouse)))
colnames(e2.mouse)[ncol(e2.mouse)-1] = "label"
colnames(e2.mouse)[ncol(e2.mouse)]   = "org"
e3.mouse = cbind(e3.mouse, rep("e3",nrow(e3.mouse)), rep("mmu",nrow(e3.mouse)))
colnames(e3.mouse)[ncol(e3.mouse)-1] = "label"
colnames(e3.mouse)[ncol(e3.mouse)]   = "org"

e2.human = cbind(e2.human, rep("e2",nrow(e2.human)), rep("hsa",nrow(e2.human)))
colnames(e2.human)[ncol(e2.human)-1] = "label"
colnames(e2.human)[ncol(e2.human)]   = "org"
e3.human = cbind(e3.human, rep("e3",nrow(e3.human)), rep("hsa",nrow(e3.human)))
colnames(e3.human)[ncol(e3.human)-1] = "label"
colnames(e3.human)[ncol(e3.human)]   = "org"
e4.human = cbind(e4.human, rep("e4",nrow(e4.human)), rep("hsa",nrow(e4.human)))
colnames(e4.human)[ncol(e4.human)-1] = "label"
colnames(e4.human)[ncol(e4.human)]   = "org"

melt.summary = rbind(e1.mouse, e2.mouse, e3.mouse,
                     e2.human, e3.human, e4.human)
pdf("total_Cells_summary.pdf", width = 13, height = 10)
ggplot(melt.summary, aes(x = original.label, y = total.cells, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(rows = vars(org))
dev.off()

pdf("total_Cells_summary_ver2.pdf", width = 13, height = 5)
ggplot(melt.summary, aes(x = org, y = total.cells, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(cols = vars(new.lab))
dev.off()

pdf("total_Cells_summary_ver2_perCellType.pdf", width = 13, height = 5)
ery.norm                  = melt.summary$ery.cnt / melt.summary$total.cells
ery.norm.filter           = melt.summary$ery.cnt > 20
ery.norm[!ery.norm.filter] = 0
ggplot(melt.summary, aes(x = org, y = ery.norm, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(cols = vars(new.lab)) + ggtitle("Ery summary")

ly.norm                  = melt.summary$ly.cnt / melt.summary$total.cells
ly.norm.filter           = melt.summary$ly.cnt > 20
ly.norm[!ly.norm.filter] = 0
ggplot(melt.summary, aes(x = org, y = ly.norm, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(cols = vars(new.lab)) + ggtitle("Ly summary")

my.norm                  = melt.summary$my.cnt / melt.summary$total.cells
my.norm.filter           = melt.summary$my.cnt > 20
my.norm[!my.norm.filter] = 0
ggplot(melt.summary, aes(x = org, y = my.norm, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(cols = vars(new.lab)) + ggtitle("My summary")

mk.norm                  = melt.summary$mk.cnt / melt.summary$total.cells
mk.norm.filter           = melt.summary$mk.cnt > 20
mk.norm[!mk.norm.filter] = 0
ggplot(melt.summary, aes(x = org, y = mk.norm, col = label)) +
  geom_boxplot() + #geom_jitter(shape=16, position=position_jitter(0.2)) +
  facet_grid(cols = vars(new.lab)) + ggtitle("MK summary")

dev.off()